const router = require('express').Router()
const User = require('../models/User')
const Post = require('../models/Post')
const { find } = require('../models/User')

//CREATE POST
router.post('/', async (req, res) => {
    const newPost = new Post(req.body)
    try {
        const savedPost = await newPost.save()
        res.status(200).json({
            status: 200,
            success: true,
            messages: 'Post Data Success...',
            data: savedPost
        })
    } catch (error) {
        res.status(500).json(error)
    }
})

//UPDATE POST
router.put('/:id', async (req, res) => {
    try {
        const post = await Post.findById(req.params.id)
        if (post.username === req.body.username) {
            try {
                const updatePost = await Post.findByIdAndUpdate(
                    req.params.id,
                    {
                        $set: req.body
                    },
                    {
                        new: true
                    }
                )
                res.status(200).json({
                    success: true,
                    messages: 'Update Data Success...',
                    data: updatePost
                })
            } catch (error) {
                res.status(500).json({
                    success: false,
                    messages: 'Internal server error!',
                    data: error
                })
            }
        } else {
            res.status(401).json({
                success: false,
                messages: 'You can update only your post!',
                data: null
            })
        }
    } catch (error) {
        res.status(500).json({
            success: false,
            messages: 'Internal server error!',
            data: error
        })
    }
})

//DELETE POST
router.delete('/:id', async (req, res) => {
    try {
        const post = await Post.findById(req.params.id)
        if (post.username === req.body.username) {
            try {
                await post.delete()
                res.status(200).json({
                    success: true,
                    messages: 'Post has been deleted...',
                    data: null
                })
            } catch (error) {
                res.status(500).json({
                    success: false,
                    messages: 'Internal server error!',
                    data: error
                })
            }
        } else {
            res.status(401).json({
                success: false,
                messages: 'You can update only your post!',
                data: null
            })
        }
    } catch (error) {
        res.status(404).json({
            success: false,
            messages: 'Data Not Found!',
            data: null
        })
    }
})

//GET POST
router.get('/:id', async (req, res) => {
    try {
        const post = await Post.findById(req.params.id)
        res.status(200).json({
            success: true,
            messages: 'Get Data Post Success...',
            data: post
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            messages: 'Data Not Found!',
            data: null
        })
    }
})

//GET ALL POST
router.get('/', async (req, res) => {
    const username = req.query.user
    const catName = req.query.cat

    console.log(req.query);

    try {
        let posts
        if (username) {
            posts = await Post.find({ username })
        } else if (catName) {
            posts = await find({
                categories: {
                    $in: [catName]
                }
            })
        } else {
            posts = await Post.find()
        }

        res.status(200).json({
            success: true,
            messages: 'Get Data Post Success...',
            data: posts
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            messages: 'Data Not Found!',
            data: null
        })
    }
})

module.exports = router