const router = require('express').Router()
const Category = require('../models/Category')

//POST
router.post('/', async (req, res) => {
    const newCat = new Category(req.body)
    try {
        const savedCat = await newCat.save()
        res.status(200).json({
            success: true,
            messages: 'Save data success...',
            data: savedCat
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            messages: 'Internal Server Error',
            data: error,
        })
    }
})

//GET
router.get('/', async (req, res) => {
    try {
        const cats = await Category.find()
        res.status(200).json({
            success: true,
            messages: 'Get data success...',
            data: cats
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            messages: 'Internal Server Error',
            data: error,
        })
    }
})

module.exports = router