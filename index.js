const express = require('express')
const app = express()
const dotenv = require('dotenv')
const mongoose = require('mongoose')
const multer = require('multer')
const path = require('path')

const authRoute = require('./src/routes/auth')
const userRoute = require('./src/routes/users')
const postRoute = require('./src/routes/posts')
const categoryRoute = require('./src/routes/categories')

dotenv.config()
app.use(express.json())
app.use('/images', express.static(path.join(__dirname, '/images')))

mongoose.connect(process.env.MONGO_URL,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: true
    }).then(
        console.log('express connect to mongodb')
    ).catch(e => console.log(e));

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, 'images')
    }, filename: (req, file, callback) => {
        console.log('tese', req.body);
        callback(null, req.body.name)
    }
})

const upload = multer({storage: storage})
app.post('/api/upload', upload.single('file'), (req, res) => {
    res.status(200).json('File has been uploaded')
})

app.use('/api/auth', authRoute)
app.use('/api/users', userRoute)
app.use('/api/posts', postRoute)
app.use('/api/categories', categoryRoute)

app.listen('5000', () => {
    console.log('Backend is running')
})